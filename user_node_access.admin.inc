<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the user_node_access module.
 */

/**
 * Form builder. Configure user node access.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function user_node_access_admin_settings() {
  // Get an array of node types with internal names as keys and render the as a from
  $form['user_node_access_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabel user node access for these content types'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('user_node_access_node_types', array('story')),
    '#description' => t('A option will be available on these content types to manage user access.'),
  );
  
  return system_settings_form($form);
}


/**
 * Get array of available private nodes
 *
 * see @user_node_access_permissions
 */
function user_node_access_get_private_nodes (){
  $query = db_query("
	  SELECT node.nid, node.title, node.type
	  FROM {node}, {node_access_type}
	  WHERE node.nid = node_access_type.nid AND
	  node_access_type.type = 1
  ");
  $nodes = array();
  while ($node = db_fetch_array($query)){
    $nodes[] = $node;
  }
  return $nodes;
}


/**
 * Form builder. User tab to Configure node access permissions.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function user_node_access_permissions(){
  //drupal_add_js(drupal_get_path('module', 'user_node_access') .'/user_node_access.js');
  //drupal_add_css(drupal_get_path('module', 'user_node_access') .'/user_node_access.css');
  
  // Get list of private nodes
  $private_nodes = user_node_access_get_private_nodes();
  
  // Get all user permissions
  $uid = arg(1);
  $user_node_access_perm = load_user_node_access_perm($uid);
  
  // Build 3 checkboxs (view, update, delete) for every private node
  foreach ($private_nodes as $node){
    $form['acl'][$node['nid']] = array('#title' => $node['title']);
    foreach (array('view', 'update', 'delete') as $op) {
    
      $form['acl'][$node['nid']][$op.'-'.$node['nid']] = array(
	    '#prefix' => '<div class="node-access-option-'.$op.'">',
		  '#suffix' => '</div>',
	    '#type' => 'checkbox',
	    '#default_value' => $user_node_access_perm[$node['nid']][$op],
		  '#class' => 'node-access-option-'.$op,
	    );
	  }
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save permissions'));
  return $form;
}


/**
 * Saving user node access permissions
 *
 * see @user_node_access_permissions()
 */
function user_node_access_permissions_submit($form, &$form_state) {
  // Merge all permissions in array
  $user_node_access_perm = array();
  foreach ($form_state['values'] as $k => $v){
		$is_op = TRUE;  	
  	if (substr($k,0,4) == 'view'){
  		$current_op = 'view';
  		$current_nid = substr($k,5,strlen($k) - 5);
 		}elseif (substr($k,0,6) == 'update'){
  		$current_op = 'update';
  		$current_nid = substr($k,7,strlen($k) - 7);
 		}elseif (substr($k,0,6) == 'delete'){
  		$current_op = 'delete';
  		$current_nid = substr($k,7,strlen($k) - 7);
 		}else{
			$is_op = FALSE; 		
 		}
 		// There is some additional values in $_POST we just need permissions values 
 		if ($is_op){
	  	$user_node_access_perm[$current_nid][$current_op] = $v;
 		}
  }
  
  // Save permissions for currect user 
  $uid = arg(1);
	foreach ($user_node_access_perm as $nid => $ops){
		// Check if acl exist if not exist create a new one
		$acl_id = acl_get_id_by_name('user_node_access','u_'. $uid .'-n_'. $nid);
  	if (!$acl_id) {
			$acl_id = acl_create_new_acl('user_node_access','u_'. $uid .'-n_'. $nid);
		}
		// Update or create permissions and rebuild node access for this node
	  acl_node_add_acl($nid, $acl_id, (int)$ops['view'], (int)$ops['update'], (int)$ops['delete'], 5);
  	acl_add_user($acl_id, $uid);
  	node_access_rebuild_by_nid($nid);
	}
	cache_clear_all();
	drupal_set_message('Permissions updated successfully!');
}

/**
 * Load Permission values for user as array
 *
 * return Array
 */
function load_user_node_access_perm($uid) {
	$user_node_access_perm = array();
	// Get all acl for the user
	$user_acls = array();
	$result = db_query("SELECT acl.acl_id
	  FROM {acl}, {acl_user}
	  WHERE acl.acl_id = acl_user.acl_id AND
	  acl.module = 'user_node_access' AND
	  uid = '%d'",$uid);

	while ($row = db_fetch_object($result)) {
    $user_acls[] = $row->acl_id;
  }
  
  // Merge all users permissions in array
	foreach ($user_acls as $acl){
		$acl_node = db_fetch_array(db_query("SELECT * FROM {acl_node} where acl_id = '%d'",$acl));
		$user_node_access_perm[$acl_node['nid']]['view'] = $acl_node['grant_view'];
		$user_node_access_perm[$acl_node['nid']]['update'] = $acl_node['grant_update'];
		$user_node_access_perm[$acl_node['nid']]['delete'] = $acl_node['grant_delete'];
	}
	return $user_node_access_perm;
}
	
	
/**
 * Theme the node permissions page.
 *
 * @ingroup themeable
 */
function theme_user_node_access_permissions($form) {

  foreach (element_children($form['acl']) as $key) {
    $row = array(); 
    $row['data'][0] = l($form['acl'][$key]['#title'],'node/'.$key);
    $row['data'][1] = drupal_render($form['acl'][$key]['view-'.$key]);
    $row['data'][2] = drupal_render($form['acl'][$key]['update-'.$key]);
    $row['data'][3] = drupal_render($form['acl'][$key]['delete-'.$key]);
    $rows[] = $row;
  }  
  $select_header = theme('table_select_header_cell');

  if (count($rows) == 0){
    $rows[] = array(array('data' => t('No private nodes available.'), 'colspan' => '6'));  	
		$select_header = '';
  }
  
  // Add checkbox to the title to allow check all functionality 
  $check_all_view = array(
		'#type' => 'checkbox',
		'#title' => t('View'),
		'#id' => 'check_all_view',
  );
  $check_all_update = array(
		'#type' => 'checkbox',
		'#title' => t('Update'),
		'#id' => 'check_all_update',
  );
  $check_all_delete = array(
		'#type' => 'checkbox',
		'#title' => t('Delete'),
		'#id' => 'check_all_delete',
  );
  
  //$header = array(t('Title'),drupal_render($check_all_view),drupal_render($check_all_update),drupal_render($check_all_delete));
  $header = array(t('Title'), t('View'), t('Update'), t('Delete'));


  $output = theme('table', $header, $rows, array('id' => 'user_node_access_permissions'));
  $output .= drupal_render($form); // Process any other fields and display them
  return $output;
}